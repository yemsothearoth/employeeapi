﻿using EmployeeManangementApi.Repo.IRepository;
using EmployeeManangementApi.Services;
using EmployeeModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManangementApi.Repo.Repository
{
    public class DepartmentRepository : IDepartmentRepository
    {
        private readonly AppContexts appDbContext;

        public DepartmentRepository(AppContexts appDbContext)
        {
            this.appDbContext = appDbContext;
        }

        public Department GetDepartment(int departmentId)
        {
            return appDbContext.Departments
                .FirstOrDefault(d => d.DepartmentId == departmentId);
        }

        public IEnumerable<Department> GetDepartments()
        {
            return appDbContext.Departments;
        }
    }
}
