﻿using EmployeeModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManangementApi.Services
{
    public class AppContexts : DbContext
    {
        public AppContexts(DbContextOptions<AppContexts> options) : base(options)
        {
        }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Department> Departments { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //Seed Departments Table
            modelBuilder.Entity<Department>().HasData(
                new Department { DepartmentId = 1, DepartmentName = "IT" });
            modelBuilder.Entity<Department>().HasData(
                new Department { DepartmentId = 2, DepartmentName = "HR" });
            modelBuilder.Entity<Department>().HasData(
                new Department { DepartmentId = 3, DepartmentName = "Payroll" });
            modelBuilder.Entity<Department>().HasData(
                new Department { DepartmentId = 4, DepartmentName = "Admin" });

            // Seed Employee Table
            modelBuilder.Entity<Employee>().HasData(new Employee
            {
                empId = 1,
                fName = "John",
                lName = "Hastings",
                email = "David@pragimtech.com",
                DOB = new DateTime(1980, 10, 5),
                Gender = Gender.Male,
                DepartmentId = 1,
                photoPath = "images/john.png"
            });

            modelBuilder.Entity<Employee>().HasData(new Employee
            {
                empId = 2,
                fName = "Sam",
                lName = "Galloway",
                email = "Sam@pragimtech.com",
                DOB = new DateTime(1981, 12, 22),
                Gender = Gender.Male,
                DepartmentId = 2,
                photoPath = "images/sam.jpg"
            });

            modelBuilder.Entity<Employee>().HasData(new Employee
            {
                empId = 3,
                fName = "Mary",
                lName = "Smith",
                email = "mary@pragimtech.com",
                DOB = new DateTime(1979, 11, 11),
                Gender = Gender.Female,
                DepartmentId = 1,
                photoPath = "images/mary.png"
            });

            modelBuilder.Entity<Employee>().HasData(new Employee
            {
                empId = 4,
                fName = "Sara",
                lName = "Longway",
                email = "sara@pragimtech.com",
                DOB = new DateTime(1982, 9, 23),
                Gender = Gender.Female,
                DepartmentId = 3,
                photoPath = "images/sara.png"
            });
        }
    }

}

