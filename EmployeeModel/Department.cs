﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EmployeeModel
{
    public class Department
    {
        [Key]
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
    }
}
