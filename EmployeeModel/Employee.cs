﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace EmployeeModel
{
    public class Employee
    {
        [Key]
        //[JsonIgnore]
        public int empId { get; set; }

        [Required]
        [MinLength(2)]
        public string fName { get; set; }
        public string lName { get; set; }
        public string email { get; set; }
        public DateTime DOB { get; set; }
        public Gender Gender { get; set; }
        public int DepartmentId { get; set; }
        //public Department Department { get; set; }
        public string photoPath { get; set; }




    }
}
